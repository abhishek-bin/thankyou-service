'use strict';
const Blog = require('../models/blog');
const Session = require('../models/session');
const BlogManager = require('../manager/blog');
const Utility = require('../../utility/util');
const AppConstant = require('../../utility/constant');

exports.getBlog = async function (req, res) {
    try {
        let getBlogResponse;
        if (req.query.blogId) {
            getBlogResponse = await BlogManager.getBlog(req.query.blogId);

            if (!getBlogResponse) {
                return Utility.response(res, {}, AppConstant.MESSAGE.BLOG_NOT_FOUND, AppConstant.NOT_FOUND);
            }
            if (getBlogResponse[0].commentMetaData && getBlogResponse[0].commentMetaData.length) {
                const indexList = getBlogResponse[0].commentMetaData.map(o => o._id.toString());
                getBlogResponse[0].commentMetaData = getBlogResponse[0].commentMetaData.filter((obj) => {
                    obj._id = obj._id.toString();
                    return obj;
                })
                getBlogResponse[0].comment = getBlogResponse[0].comment.filter((obj) => {
                    obj.userId = obj.userId.toString();
                    return obj;
                })
                console.log(getBlogResponse[0].commentMetaData);
                getBlogResponse[0].comment = getBlogResponse[0].comment.filter((o) => {
                    console.log(getBlogResponse[0].commentMetaData[indexList.indexOf(o.userId)])
                    o.name = getBlogResponse[0].commentMetaData[indexList.indexOf(o.userId)].name;
                    o.profileUrl = getBlogResponse[0].commentMetaData[indexList.indexOf(o.userId)].profileUrl;
                    return o;
                });
                delete getBlogResponse[0].commentMetaData;
            }
            return Utility.response(res, getBlogResponse[0], AppConstant.MESSAGE.BLOG_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);
        }
        getBlogResponse = await BlogManager.getBlog('');
        return Utility.response(res, getBlogResponse, AppConstant.MESSAGE.BLOG_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);
    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_GET_BLOG, AppConstant.FAILURE);
    }

};


exports.addBlog = async function (req, res) {
    try {
        const blog = new Blog(req.body);
        const addBlogResponse = await BlogManager.addBlog(blog);

        if (!addBlogResponse) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_ADD_BLOG, AppConstant.CONFLICT);
        }
        return Utility.response(res, addBlogResponse, AppConstant.MESSAGE.BLOG_ADDED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e);
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_ADD_BLOG, AppConstant.FAILURE);
    }

};

exports.likeBlog = async function (req, res) {
    try {
        const reqObj = {
            blogId: req.params.blogId
        };
        const getBlogResponse = await BlogManager.getBlog(reqObj.blogId);
        if (!getBlogResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.NO_PRODUCT_FOUND, AppConstant.NOT_FOUND);
        }
        reqObj.userId = req.user.id;
        console.log(getBlogResponse)
        getBlogResponse[0].like = getBlogResponse[0].like.map(o => o.toString())
        console.log(getBlogResponse[0].like.includes(req.user.id))
        if (getBlogResponse[0].like.includes(req.user.id)) {
            const likeBlogResponse = await BlogManager.unlikeBlog(reqObj);
            return Utility.response(res, {}, AppConstant.MESSAGE.PRODUCT_LIKED_SUCCESSFULLY, AppConstant.SUCCESS);

        }

        const likeBlogResponse = await BlogManager.likeBlog(reqObj);

        return Utility.response(res, {}, AppConstant.MESSAGE.PRODUCT_LIKED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABEL_TO_LIKE_PRODUCT, AppConstant.FAILURE);
    }

};

exports.commentBlog = async function (req, res) {
    try {
        const reqObj = {
            blogId: req.body.blogId
        };
        const getProductResponse = await BlogManager.getBlog(reqObj.blogId);
        if (!getProductResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.BLOG_NOT_FOUND, AppConstant.NOT_FOUND);
        }
        reqObj.userId = req.body.userId;
        reqObj.text = req.body.text;
        reqObj.addedOn = Date.now();


        const commentProductResponse = await BlogManager.commentBlog(reqObj);
        // if (!likeProductResponse.nModified) {
        //     return Utility.response(res, {}, AppConstant.MESSAGE.UNABEL_TO_LIKE_PRODUCT, AppConstant.FAILURE);
        //
        // }
        return Utility.response(res, {}, AppConstant.MESSAGE.COMMENT_ADDED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABEL_TO_LIKE_PRODUCT, AppConstant.FAILURE);
    }

};



