const Utility = require('../../utility/util');
const AppConstant = require('../../utility/constant');
const Donate = require('../models/donation');
const Payment = require('../models/payment');
const DonationManager = require('../manager/donate');

module.exports = {
    paymentStatus: async (req, res) => {
        try {
            const payment = new Payment(req.body);
            payment.addedOn = Date.now();
            const donationResponse = await DonationManager.addPayment(payment);
            if (!donationResponse) {
                return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_POST_DONATION, AppConstant.FAILURE);
            }
            return Utility.response(res, {}, AppConstant.MESSAGE.DONATION_POSTED, AppConstant.SUCCESS);

        } catch (e) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_POST_DONATION, AppConstant.FAILURE);

        }
    },
    addDonation: async (req, res) => {
        try {
            const donate = new Donate(req.body);
            console.log(req.body)
            console.log(req.user)
            if (req.user.id !== req.body.addedBy) {
                return Utility.response(res, {}, AppConstant.MESSAGE.NOT_ALLOWED_OP, AppConstant.NOT_ALLOWED);
            }
            if (req.user.email !== AppConstant.ADMIN_EMAIL) {
                return Utility.response(res, {}, AppConstant.MESSAGE.NOT_ALLOWED_OP, AppConstant.NOT_ALLOWED);
            }
            donate.addedOn = Date.now();
            const donationResponse = await DonationManager.addDonation(donate);

            if (!donationResponse) {
                return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_POST_DONATION, AppConstant.FAILURE);
            }
            return Utility.response(res, {}, AppConstant.MESSAGE.DONATION_POSTED, AppConstant.SUCCESS);

        } catch (e) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_POST_DONATION, AppConstant.FAILURE);

        }
    },
    deleteDonation: async (req, res) => {
        try {
            if (req.user.id !== req.body.addedBy) {
                return Utility.response(res, {}, AppConstant.MESSAGE.NOT_ALLOWED, AppConstant.NOT_ALLOWED);
            }
            const donation = await DonationManager.getDonation(req.params.donationId);
            if (!donation.length) {
                return Utility.response(res, {}, AppConstant.MESSAGE.NOT_FOUND, AppConstant.NOT_FOUND);
            }
            const donationResponse = await DonationManager.updateDonation({
                donationId: req.params.donationId,
                isDeleted: 1
            });

            return Utility.response(res, {}, AppConstant.MESSAGE.DONATION_DELETED, AppConstant.SUCCESS);

        } catch (e) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_POST_DONATION, AppConstant.FAILURE);

        }
    },
    getDonation: async (req, res) => {
        try {
            if (req.query.donationId) {
                let donation = await DonationManager.getDonation(req.query.donationId);
                donation = donation[0];
                return Utility.response(res, donation, AppConstant.MESSAGE.DONATION_FETCHED, AppConstant.SUCCESS);
            }
            const donation = await DonationManager.getDonation();

            return Utility.response(res, donation, AppConstant.MESSAGE.DONATION_FETCHED, AppConstant.SUCCESS);

        } catch (e) {
            console.log(e)
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_POST_DONATION, AppConstant.FAILURE);

        }
    },
    getNotification: async (req, res) => {
        try {

            const donation = await DonationManager.getNotif(req.params.userId);

            return Utility.response(res, donation, AppConstant.MESSAGE.DATA_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);

        } catch (e) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_FETCH_USER, AppConstant.FAILURE);

        }
    },
    getNotificationCnt: async (req, res) => {
        try {

            const notificationCount = await DonationManager.getNotifCount(req.params.userId);
            const messageCount = await DonationManager.getMsgCount(req.params.userId);

            return Utility.response(res, {
                notificationCount,
                messageCount
            }, AppConstant.MESSAGE.DATA_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);

        } catch (e) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_FETCH_USER, AppConstant.FAILURE);

        }
    },
    markReadNotification: async (req, res) => {
        try {
            const donation = await DonationManager.updateNotif(req.params.notificationId);
            const noti = await DonationManager.getNotif(req.user.id);

            return Utility.response(res, noti, AppConstant.MESSAGE.DONATION_FETCHED, AppConstant.SUCCESS);

        } catch (e) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_POST_DONATION, AppConstant.FAILURE);

        }
    }
}
