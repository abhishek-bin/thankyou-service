'use strict';
const Message = require('../models/message');
const UserManager = require('../manager/user');
const MessageManager = require('../manager/message');
const Utility = require('../../utility/util');
const AppConstant = require('../../utility/constant');
const _ = require('lodash')

exports.getChatUser = async (req, res) => {
    try {
        const userId = req.params.userId;
        /*Will add the logic to fetch chat users, right now fetching all the user in db with role as '2'*/
        const getChatUserResponse = await UserManager.getChatUser({userId});
        return Utility.response(res, getChatUserResponse, AppConstant.MESSAGE.USERS_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);
    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_FETCH_USER, AppConstant.FAILURE);
    }
}
exports.deleteChat = async (req, res) => {
    try {
        const reqObj = {
            userId: req.user.id,
            receiverList: req.body.receiverList
        }
        /*Will add the logic to fetch chat users, right now fetching all the user in db with role as '2'*/
        const getChatUserResponse = await UserManager.deleteChat(reqObj);
        return Utility.response(res, getChatUserResponse, AppConstant.MESSAGE.USERS_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);
    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_FETCH_USER, AppConstant.FAILURE);
    }
}
exports.getChatHistory = async (req, res) => {
    try {
        /*Will add the logic to fetch chat users, right now fetching all the user in db with role as '2'*/
        const getChatUserResponseS = await MessageManager.getChatHistoryS({
            userId: req.params.userId
        });
        const getChatUserResponseR = await MessageManager.getChatHistoryR({
            userId: req.params.userId
        });
        let latestChat = getChatUserResponseS.concat(getChatUserResponseR);
        const userList = [];
        for(let index = 0; index < latestChat.length; index++){
            let obj = {};
            obj._id = latestChat[index]._id._id;
            obj.addedOn = latestChat[index].addedOn.sort().pop();
            userList.push(obj);
        }
        const dataList = JSON.parse(JSON.stringify(getChatUserResponseS.concat(getChatUserResponseR).map(item => item._id)))
        let resList = _.uniqBy(dataList, '_id');
        // console.log(userList)
        const chatMsgPromiseList = resList.map((o) => {
            o.userId = req.params.userId;
            return MessageManager.UnreadMessageCount(o);
        });
        const countList = await Promise.all(chatMsgPromiseList);
        // console.log(resList)
        for (let i = 0; i < resList.length; i++) {
            resList[i].count = countList[i];
            console.log(resList[i]._id);
            console.log(userList)
            resList[i].addedOnChat = userList[i].addedOn;
        }
        console.log(resList);
        resList = _.orderBy(resList, ['addedOnChat'],['desc']);
        // console.log(resList);

        return Utility.response(res, resList, AppConstant.MESSAGE.MESSAGE_FETCHED, AppConstant.SUCCESS);
    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_FETCH_MESSAGE, AppConstant.FAILURE);
    }
}
exports.getChatMessage = async (req, res) => {
    try {
        /*Will add the logic to fetch chat users, right now fetching all the user in db with role as '2'*/
        const getChatUserResponse = await MessageManager.getMessage({
            senderId: req.params.senderId,
            receiverId: req.params.receiverId,
            epoch: req.params.epoch,
        });
        console.log(getChatUserResponse)
        return Utility.response(res, getChatUserResponse, AppConstant.MESSAGE.MESSAGE_FETCHED, AppConstant.SUCCESS);
    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_FETCH_MESSAGE, AppConstant.FAILURE);
    }
}
exports.markReadMessage = async (req, res) => {
    try {
        /*Will add the logic to fetch chat users, right now fetching all the user in db with role as '2'*/
        const getChatUserResponse = await MessageManager.markRead({
            senderId: req.params.senderId,
            receiverId: req.user.id,
        });
        console.log(getChatUserResponse)
        return Utility.response(res, getChatUserResponse, AppConstant.MESSAGE.MESSAGE_FETCHED, AppConstant.SUCCESS);
    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_FETCH_MESSAGE, AppConstant.FAILURE);
    }
}
exports.setChatMessage = async (req, res) => {
    try {
        const msg = new Message(req.body);
        msg.senderId = req.body.senderId;
        msg.receiverId = req.body.receiverId;
        msg.isRead = 0;
        msg.addedOn = Date.now();
        const userPromise = UserManager.getUserById(msg.receiverId);
        const recvPromise = UserManager.getUserById(msg.senderId);
        const getChatUserResponse = await MessageManager.saveMessage(msg);
        const userRes = await userPromise;
        const recvRes = await recvPromise;
        if (userRes.length && userRes[0].deviceToken) {

            Utility.sendPush({
                title: recvRes[0].name,
                body: msg.message,
                deviceToken: userRes[0].deviceToken,
                data: JSON.parse(JSON.stringify({
                    senderId: msg.senderId,
                    notificationType: "chat-message",
                }))
            });
        }

        if (!getChatUserResponse) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_SEND_MESSAGE, AppConstant.FAILURE);
        }
        return Utility.response(res, getChatUserResponse, AppConstant.MESSAGE.MESSAGE_SAVED, AppConstant.SUCCESS);
    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_SEND_MESSAGE, AppConstant.FAILURE);
    }
}
