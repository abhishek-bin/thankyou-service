'use strict';
const Product = require('../models/product');
const Rate = require('../models/rating');
const Notification = require('../models/notifications');
const Message = require('../models/message');
const ProductManager = require('../manager/product');
const MessageManager = require('../manager/message');
const UserManager = require('../manager/user');
const Utility = require('../../utility/util');
const AppConstant = require('../../utility/constant');

exports.addProduct = async function (req, res) {
    try {
        const product = new Product(req.body);
        product.location.coordinates = req.body.location;
        product.location.type = "Point";
        const addProductResponse = await ProductManager.addProduct(product);

        if (!addProductResponse) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_ADD_PRODUCT, AppConstant.CONFLICT);
        }
        return Utility.response(res, addProductResponse, AppConstant.MESSAGE.PRODUCT_ADDED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_ADD_PRODUCT, AppConstant.FAILURE);
    }

};
exports.test = async function (req, res) {
    // let userList = await UserManager.getActiveToken({
    //     userId: '5f5bd9630ee90558dc98c09c', location: [
    //         83.3901199,
    //         26.8093446
    //     ]
    // });
    // return Utility.response(res, userList, AppConstant.MESSAGE.UNABLE_TO_ADD_PRODUCT, AppConstant.SUCCESS);
}
exports.donate = async function (req, res) {
    try {
        console.log(req.body)
        const product = new Product(req.body);
        product.location.coordinates = req.body.location;
        product.location.type = "Point";
        product.isDonated = 1;
        product.addedOn = Date.now();
        console.log(product);
        const userResponse = await UserManager.getPublicUserById(req.body.addedBy);
        if (!userResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.USER_NOT_REGISTERED, AppConstant.NOT_FOUND);

        }
        const addProductResponse = await ProductManager.addProduct(product);

        if (!addProductResponse) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_ADD_PRODUCT, AppConstant.CONFLICT);
        }
        let userList = await UserManager.getActiveToken({
            userId: req.body.addedBy,
            location: product.location.coordinates
        });
        console.log(userList);
        const userIdList = [...userList];
        userList = userList.map(o => o.deviceToken);
        if (userList.length) {
            userList = userList.map((token, index) => {
                return new Promise(((resolve, reject) => {
                    const obj = {
                        title: 'Shared',
                        body: `${userResponse.length ? userResponse[0].name : 'Someone'} has shared. Check Now!`,
                        deviceToken: token,
                        data: JSON.parse(JSON.stringify({
                            productId: product._id,
                            notificationType: "listing"
                        }))
                    }
                    Utility.sendPush(obj);
                    obj.data = JSON.stringify(obj.data);
                    const notification = new Notification(obj);
                    notification.addedOn = Date.now();
                    notification.receiverId = userIdList[index]._id.toString();
                    ProductManager.saveNotif(notification)
                    resolve(1);
                }))
            });
            await Promise.all(userList);
        }
        return Utility.response(res, addProductResponse, AppConstant.MESSAGE.PRODUCT_DONATED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_ADD_PRODUCT, AppConstant.FAILURE);
    }

};
exports.want = async function (req, res) {
    try {
        const product = new Product(req.body);
        product.location.coordinates = req.body.location;
        product.location.type = "Point";

        product.addedOn = Date.now();
        const getDonateHistoryList = await ProductManager.getProductByUserId(req.user.id);

        const userResponse = await UserManager.getPublicUserById(req.user.id);
        if (!userResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.USER_NOT_REGISTERED, AppConstant.NOT_FOUND);

        }
        if (!getDonateHistoryList.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.NOT_DONATED, AppConstant.CONFLICT);

        }
        const addProductResponse = await ProductManager.addProduct(product);

        if (!addProductResponse) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_ADD_PRODUCT, AppConstant.CONFLICT);
        }
        let userList = await UserManager.getActiveToken({userId: req.body.addedBy, location: product.location.coordinates});
        console.log(userList);
        const userIdList = [...userList];
        userList = userList.map(o => o.deviceToken);
        if (userList.length) {
            userList = userList.map((token, index) => {
                return new Promise(((resolve, reject) => {
                    const obj = {
                        title: 'Wanted',
                        body: `${userResponse[0].name} has posted their want!`,
                        deviceToken: token,
                        data: JSON.parse(JSON.stringify({
                            productId: product._id,
                            notificationType: "listing"
                        }))
                    };
                    Utility.sendPush(obj);
                    obj.data = JSON.stringify(obj.data);
                    const notification = new Notification(obj);
                    notification.addedOn = Date.now();
                    notification.receiverId = userIdList[index]._id.toString();
                    console.log(notification)
                    ProductManager.saveNotif(notification)
                    resolve(1);
                }))
            });
            await Promise.all(userList);
        }
        return Utility.response(res, addProductResponse, AppConstant.MESSAGE.PRODUCT_ADDED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_ADD_PRODUCT, AppConstant.FAILURE);
    }

};

exports.requestProduct = async function (req, res) {
    try {
        const getProductResponse = await ProductManager.getProductById(req.params.productId);
        console.log(getProductResponse);
        if (!getProductResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.NOT_FOUND, AppConstant.FAILURE);
        }
        const userRes = await UserManager.getUserById(req.user.id);
        if (!userRes.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.NOT_FOUND, AppConstant.FAILURE);
        }
        const msg = new Message();
        msg.senderId = req.user.id;
        msg.message = `Hi! I am interested in ${getProductResponse[0].title} - ${getProductResponse[0].description}. Lets discuss more!`;
        msg.receiverId = getProductResponse[0].userId;
        msg.isRead = 0;
        msg.addedOn = Date.now();
        await MessageManager.saveMessage(msg);
        if (getProductResponse[0].deviceToken) {
            const reqObj = {
                title: 'Request',
                body: `${userRes[0].name} wants your shared item !`,
                deviceToken: getProductResponse[0].deviceToken,

                data: {
                    senderId: req.user.id,
                    notificationType: 'request-listing',
                }
            }
            await Utility.sendPush(reqObj);
            const notification = new Notification(reqObj);
            notification.addedOn = Date.now();
            notification.receiverId = getProductResponse[0]._id;
            ProductManager.saveNotif(notification)
            return Utility.response(res, {}, AppConstant.MESSAGE.RAISED_SUCCESS, AppConstant.SUCCESS);
        }
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_RAISE, AppConstant.FAILURE);
    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_GET_PRODUCT, AppConstant.FAILURE);
    }

};
exports.relatedProduct = async function (req, res) {
    try {
        const getProductResponse = await ProductManager.getProductByCategory({
            userId: req.user.id,
            category: req.params.category,
            productId: req.params.productId
        });
        return Utility.response(res, getProductResponse, AppConstant.MESSAGE.DATA_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_GET_PRODUCT, AppConstant.FAILURE);
    }

};

exports.productByMe = async function (req, res) {
    try {
        const getProductResponse = await ProductManager.getProductByMe({
            userId: req.params.addedBy,
            productId: req.params.productId
        });
        let rating = 0;
        const getProduct = await ProductManager.getProductByUserId(req.params.addedBy);
        const productIdList = getProduct.map(o => o._id);

        const getRatingData = await ProductManager.getRatingByUserId({productIdList});
        if (getRatingData.length) {
            console.log(getRatingData)
            rating = getRatingData.map(o => o.value).reduce(function (a, b) {
                return a + b;
            }, 0);
            rating = rating / getRatingData.length;
        }
        return Utility.response(res, {
            product: getProductResponse,
            rating: rating
        }, AppConstant.MESSAGE.DATA_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_GET_PRODUCT, AppConstant.FAILURE);
    }

};
exports.getProductById = async function (req, res) {
    try {
        const getProductResponsePromise = ProductManager.getProductById(req.params.productId);
        const getSelfRatingResponsePromise = ProductManager.getRating({
            productId: req.params.productId,
            userId: req.user.id
        });

        let getProductResponse = await getProductResponsePromise;
        const getSelfRatingResponse = await getSelfRatingResponsePromise;
        getProductResponse = getProductResponse.length ? getProductResponse[0] : {}
        getProductResponse.selfRating = getSelfRatingResponse.length && getSelfRatingResponse[0].value ? getSelfRatingResponse[0].value : 0;
        return Utility.response(res, getProductResponse, AppConstant.MESSAGE.DATA_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_GET_PRODUCT, AppConstant.FAILURE);
    }

};

exports.getMyProductByUserId = async function (req, res) {
    try {
        const getProductResponse = await ProductManager.getProductByUserId(req.params.userId);
        console.log(getProductResponse)
        return Utility.response(res, getProductResponse, AppConstant.MESSAGE.DATA_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_GET_PRODUCT, AppConstant.FAILURE);
    }

};
exports.getProduct = async function (req, res) {
    try {
        const reqObj = {
            userId: req.body.userId,
            location: req.body.location
        }
        console.log(reqObj)
        const getProductListResponse = await ProductManager.getProductExist(reqObj);
        if (!getProductListResponse.length) {
            return Utility.response(res, [], AppConstant.MESSAGE.PRODUCT_ADDED_SUCCESSFULLY, AppConstant.SUCCESS);

        }
        const getProductResponse = await ProductManager.getProduct(reqObj);


        return Utility.response(res, getProductResponse, AppConstant.MESSAGE.PRODUCT_ADDED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e);
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_GET_PRODUCT, AppConstant.FAILURE);
    }

};
exports.updateProductStatus = async function (req, res) {
    try {
        const reqObj = {
            productId: req.params.productId
        }
        if (req.query.isDeleted) {
            reqObj.isDeleted = 1;
        } else if (req.query.status) {
            reqObj.status = req.query.status;
        } else {
            return Utility.response(res, [], AppConstant.MESSAGE.NO_DATA_TO_UPDATE, AppConstant.SUCCESS);

        }
        const getProductResponse = await ProductManager.updateProduct(reqObj);
        return Utility.response(res, {}, AppConstant.MESSAGE.PRODUCT_ADDED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e);
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_GET_PRODUCT, AppConstant.FAILURE);
    }

};
exports.likeProduct = async function (req, res) {
    try {
        const reqObj = {
            productId: req.params.productId
        };
        const getProductResponse = await ProductManager.getProductByIdPlain(reqObj.productId);
        if (!getProductResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.NO_PRODUCT_FOUND, AppConstant.NOT_FOUND);
        }
        reqObj.userId = req.user.id;
        if (getProductResponse[0].like.includes(req.user.id)) {
            const likeProductResponse = await ProductManager.unlikeProduct(reqObj);
            return Utility.response(res, {}, AppConstant.MESSAGE.PRODUCT_LIKED_SUCCESSFULLY, AppConstant.SUCCESS);

        }

        const likeProductResponse = await ProductManager.likeProduct(reqObj);

        return Utility.response(res, getProductResponse, AppConstant.MESSAGE.PRODUCT_LIKED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABEL_TO_LIKE_PRODUCT, AppConstant.FAILURE);
    }

};
exports.rateProduct = async function (req, res) {
    try {
        const reqObj = {
            productId: req.params.productId,
            value: req.params.value
        };

        const getProductResponse = await ProductManager.getProductByIdPlain(reqObj.productId);
        if (!getProductResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.NO_PRODUCT_FOUND, AppConstant.NOT_FOUND);
        }
        if (req.user.id === getProductResponse[0].addedBy.toString()) {
            return Utility.response(res, {}, AppConstant.MESSAGE.CANNOT_RATE_OWN_PRODUCT, AppConstant.FAILURE);

        }

        reqObj.userId = req.user.id;
        const getRating = await ProductManager.getRating(reqObj);

        if (getRating.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.ALREADY_RAYED, AppConstant.FAILURE);

        }
        const rating = new Rate(reqObj);
        const likeProductResponse = await ProductManager.saveRating(rating);

        return Utility.response(res, {}, AppConstant.MESSAGE.RATED_SUCCESS, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABEL_TO_LIKE_PRODUCT, AppConstant.FAILURE);
    }

};
