'use strict';
const User = require('../models/user');
const randomatic = require('randomatic');
const Session = require('../models/session');
const Feedback = require('../models/feedback');
const UserManager = require('../manager/user');
const Utility = require('../../utility/util');
const AppConstant = require('../../utility/constant');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.updateUser = async function (req, res) {
    try {
        const reqObj = {
            userId: req.body.userId,
        };
        if (req.body.name) {
            reqObj.name = req.body.name;
        }
        if (req.body.phone) {
            reqObj.phone = req.body.phone;
            reqObj.isVerified = 0;
            const userRes = await UserManager.getUserByPhone({phone: reqObj.phone});
            if (userRes.length) {
                return Utility.response(res, {}, AppConstant.MESSAGE.NUMBER_IN_USE, AppConstant.NOT_FOUND);
            }
        }
        if (req.body.dob) {
            reqObj.dob = req.body.dob;
        }
        if (req.body.address) {
            reqObj.address = req.body.address;
        }
        if (req.body.zipCode) {
            reqObj.zipCode = req.body.zipCode;
        }
        if (req.body.profileUrl) {
            reqObj.profileUrl = req.body.profileUrl;
        }
        if (req.body.radius) {
            reqObj.radius = req.body.radius * 1000;
        }
        let getUserByEmailResponse = await UserManager.getUserById(reqObj.userId)
        if (!getUserByEmailResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.USER_NOT_REGISTERED, AppConstant.NOT_FOUND);
        }
        // reqObj.userId = req.user.id;
        const userUpdateResponse = await UserManager.updateUser(reqObj)
        return Utility.response(res, userUpdateResponse, AppConstant.MESSAGE.USER_UPDATED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_LOGIN, AppConstant.FAILURE);
    }
};
exports.verifyOtp = async (req, res) => {
    try {
        const userObj = {};
        userObj.otp = req.body.otp;
        userObj.phone = req.body.phone;
        let getOtpResponseByPhone = await UserManager.getUserByPhone(userObj);
        if (!getOtpResponseByPhone.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.USER_NOT_REGISTERED, AppConstant.NOT_FOUND);
        }
        console.log(req.body.otp, getOtpResponseByPhone[0].otp)
        const matchResult = await bcrypt.compare(req.body.otp, getOtpResponseByPhone[0].otp);
        if (!matchResult) {
            return Utility.response(res, {}, AppConstant.MESSAGE.OTP_MISMATCH, AppConstant.CONFLICT);
        }
        let userUpdateResponse = await UserManager.updateUser({userId: getOtpResponseByPhone[0]._id, isVerified: 1})

        userUpdateResponse = userUpdateResponse.toObject();
        userUpdateResponse.token = await generateTokenAndLogin(userUpdateResponse, res);
        // delete getOtpResponseByPhone.password;
        return Utility.response(res, userUpdateResponse, AppConstant.MESSAGE.USER_REGISTERED_SUCCESSFULLY, AppConstant.SUCCESS);

        return Utility.response(res, {}, AppConstant.MESSAGE.OTP_VERIFIED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e);
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_VERIFY, AppConstant.FAILURE);
    }
};
exports.resetPassword = async (req, res) => {
    try {
        const userObj = {};
        userObj.otp = req.body.otp;
        userObj.phone = req.body.phone;
        userObj.password = req.body.password;

        let getOtpResponseByPhone = await UserManager.getUserByPhone(userObj);
        if (!getOtpResponseByPhone.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.USER_NOT_REGISTERED, AppConstant.NOT_FOUND);
        }
        console.log(req.body.otp, getOtpResponseByPhone[0].otp)
        const matchResult = await bcrypt.compare(req.body.otp, getOtpResponseByPhone[0].otp);
        if (!matchResult) {
            return Utility.response(res, {}, AppConstant.MESSAGE.OTP_MISMATCH, AppConstant.CONFLICT);
        }
        let userUpdateResponse = await UserManager.updateUser({
            userId: getOtpResponseByPhone[0]._id,
            password: bcrypt.hashSync(userObj.password, AppConstant.SALT.ROUNDS)
        })


        return Utility.response(res, {}, AppConstant.MESSAGE.PASSWORD_RESET_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e);
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_VERIFY, AppConstant.FAILURE);
    }
};

exports.sendOtp = async function (req, res) {
    try {
        const reqObj = {
            phone: req.params.phone,
        };
        let getUserByEmailResponse = await UserManager.getUserByEmailAndPhone(reqObj);
        if (!getUserByEmailResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.USER_NOT_REGISTERED, AppConstant.NOT_FOUND);
        }
        // reqObj.userId = req.user.id;
        let otp = randomatic('0', 6);
        await Utility.sendSms({body: `OTP to verify your eeho account is ${otp}`, phone: reqObj.phone});
        // await Utility.sendMail({
        //     subject: 'EEho Account Verification',
        //     body: `OTP to verify your eeho account is ${otp}`,
        //     email: getUserByEmailResponse[0].email
        // });
        otp = bcrypt.hashSync(otp, AppConstant.SALT.ROUNDS);

        const userUpdateResponse = await UserManager.updateUser({otp: otp, userId: getUserByEmailResponse[0]._id});
        return Utility.response(res, {}, AppConstant.MESSAGE.OTP_SENT, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_LOGIN, AppConstant.FAILURE);
    }
};
exports.resetOtp = async function (req, res) {
    try {
        const reqObj = {
            phone: req.params.phone,
        };
        let getUserByEmailResponse = await UserManager.getUserByEmailAndPhone(reqObj);
        if (!getUserByEmailResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.USER_NOT_REGISTERED, AppConstant.NOT_FOUND);
        }
        // reqObj.userId = req.user.id;
        let otp = randomatic('0', 6);
        await Utility.sendSms({body: `OTP to reset your eeho account password is ${otp}`, phone: reqObj.phone});
        // await Utility.sendMail({
        //     subject: 'EEho Account Verification',
        //     body: `OTP to verify your eeho account is ${otp}`,
        //     email: getUserByEmailResponse[0].email
        // });
        otp = bcrypt.hashSync(otp, AppConstant.SALT.ROUNDS);

        const userUpdateResponse = await UserManager.updateUser({otp: otp, userId: getUserByEmailResponse[0]._id});
        return Utility.response(res, {}, AppConstant.MESSAGE.OTP_SENT, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_LOGIN, AppConstant.FAILURE);
    }
};
exports.needHelp = async function (req, res) {
    try {
        const reqObj = {
            userId: req.body.userId,
            subject: req.body.subject,
            description: req.body.description,
        };
        let getUserByEmailResponse = await UserManager.getUserById(reqObj.userId);
        if (!getUserByEmailResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.USER_NOT_REGISTERED, AppConstant.NOT_FOUND);
        }
        await Utility.sendMail({
            subject: 'eeho - Need Help',
            // body: `<h2>A help is need to ${getUserByEmailResponse[0].name}</h2>
            //         <div>
            //         <hr>
            //         <h2>Subject : ${reqObj.subject}</h2></div>
            //          <div>
            //
            //         <h3>Description : ${reqObj.description}</h3></div>
            //         <hr>
            //         <p>User Details:</p>
            //          <p>Mail : ${getUserByEmailResponse[0].email}</p>
            //          <p>Phone : ${getUserByEmailResponse[0].phone}</p>
            //          <p>Address : ${getUserByEmailResponse[0].address}</p>
            //              `,
            body: `<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
<style>
    *{
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    html,body{
        background: #eeeeee;
        font-family: 'Open Sans', sans-serif, Helvetica, Arial;
    }
    img{
        max-width: 100%;
    }
    /* This is what it makes reponsive. Double check before you use it! */
    @media only screen and (max-width: 480px){
        table tr td{
            width: 100% !important;
            float: left;
        }
    }
</style>
</head>

<body style="background: #eeeeee; padding: 10px; font-family: 'Open Sans', sans-serif, Helvetica, Arial;">

<!-- ** Table begins here
----------------------------------->
<!-- Set table width to fixed width for Outlook(Outlook does not support max-width) -->
<table width="100%" cellpadding="0" cellspacing="0" bgcolor="FFFFFF" style="background: #ffffff; max-width: 600px !important; margin: 0 auto; background: #ffffff;">
    <tr>
        <td style="padding: 20px; text-align: center; background: #76ce3e;">
            <h1 style="color: #ffffff">${getUserByEmailResponse[0].name} - Needs eeho Help</h1>
        </td>
    </tr>


    <tr>
        <td style="padding: 20px; text-align: center;">
            <!-- ** 100% width
            ----------------------------------->
            <p style="font-size:22px; margin: 5px;">Subject - ${reqObj.subject}</p>
            <p style="font-size:22px;">Description - ${reqObj.description}</p>
        </td>
    </tr>
    <tr>
        <td style="padding: 20px; background: #2B2E34;">

            <!-- ** 50% and 50%
            ----------------------------------->
            <table border="0" cellpadding="0" cellspacing="0" a>
                <tr>
                    <td width="50%" style="width: 50%; padding: 10px; color: #ffffff; text-align: left;" valign="top">
                        <h2>User's Address</h2>
                        <p style="font-size: 14px;">${getUserByEmailResponse[0].address}</p>
                    </td>
                    <td width="50%" style="width: 50%; padding: 10px; color: #ffffff; text-align: left;" valign="top">
                        <h2>Contact Information</h2>
                        <!-- ** Footer contact
                        ----------------------------------->
                        <table border="0" style="font-size: 14px;">
                            <tr><td>Phone: ${getUserByEmailResponse[0].phone}</td></tr>
                            <tr><td>Email:  ${getUserByEmailResponse[0].email}</td></tr>
                        </table>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>


<!-- ** Bottom Message
----------------------------------->
<p style="text-align: center; color: #666666; font-size: 12px; margin: 10px 0;">
    Copyright © 2020. All&nbsp;rights&nbsp;reserved.<br />
</p>

</center>

</body>
</html>`,
            email: AppConstant.CONTACT.EMAIL
        });
        return Utility.response(res, {}, AppConstant.MESSAGE.QUERY_RAISED, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_LOGIN, AppConstant.FAILURE);
    }
};
exports.login = async function (req, res) {
    try {
        const reqObj = {
            email: req.body.email.toLowerCase(),
            password: req.body.password
        };
        let getUserByEmailResponse = await UserManager.getUserByEmailAndPhone({
            email: reqObj.email,
        });

        if (!getUserByEmailResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.USER_NOT_REGISTERED, AppConstant.NOT_FOUND);
        }
        if (getUserByEmailResponse[0].fbId.length || getUserByEmailResponse[0].gmailId.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.TRY_SOCIAL_LOGIN, AppConstant.NOT_ALLOWED);
        }
        if (!getUserByEmailResponse[0].isVerified) {
            return Utility.response(res, getUserByEmailResponse[0], AppConstant.MESSAGE.USER_NOT_VERIFIED, AppConstant.CONFLICT);
        }
        console.log(req.body.password, getUserByEmailResponse[0].password)
        const matchResult = await bcrypt.compare(req.body.password, getUserByEmailResponse[0].password);
        if (!matchResult) {
            return Utility.response(res, {}, AppConstant.MESSAGE.INVALID_CREDENTIALS, AppConstant.UNAUTHORIZED);
        }
        getUserByEmailResponse = getUserByEmailResponse[0].toObject();
        getUserByEmailResponse.token = await generateTokenAndLogin(getUserByEmailResponse, res);
        delete getUserByEmailResponse.password;
        return Utility.response(res, getUserByEmailResponse, AppConstant.MESSAGE.USER_SINGED_IN_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_LOGIN, AppConstant.FAILURE);
    }
};
exports.logout = async function (req, res) {
    try {
        await UserManager.updateUser({userId: req.params.userId, deviceToken: ''});

        return Utility.response(res, {}, AppConstant.MESSAGE.LOGOUT_SUCCESS, AppConstant.SUCCESS);

    } catch (e) {
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_LOGIN, AppConstant.FAILURE);
    }
};

exports.fbLogin = async function (req, res) {
    try {
        const user = new User(req.body);
        console.log(req.body)
        console.log(user);
        let getUserByEmailResponse = await UserManager.getUserByEmail(user);
        if (getUserByEmailResponse.length) {
            getUserByEmailResponse = getUserByEmailResponse[0].toObject();
            getUserByEmailResponse.token = await generateTokenAndLogin(getUserByEmailResponse, res);
            delete getUserByEmailResponse.password;
            return Utility.response(res, getUserByEmailResponse, AppConstant.MESSAGE.USER_REGISTERED_SUCCESSFULLY, AppConstant.SUCCESS);
        }

        user.location.coordinates = req.body.location;
        user.location.type = "Point";
        let registerUserResponse = await UserManager.registerUser(user);
        console.log(registerUserResponse);
        registerUserResponse = registerUserResponse.toObject();
        if (!registerUserResponse) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_REGISTER, AppConstant.FAILURE);
        }
        registerUserResponse.token = await generateTokenAndLogin(registerUserResponse, res);
        delete registerUserResponse.password;
        return Utility.response(res, registerUserResponse, AppConstant.MESSAGE.USER_REGISTERED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_REGISTER, AppConstant.FAILURE);
    }

};
exports.gmailLogin = async function (req, res) {
    try {
        const user = new User(req.body);
        let getUserByEmailResponse = await UserManager.getUserByEmail(user);
        if (getUserByEmailResponse.length) {
            getUserByEmailResponse = getUserByEmailResponse[0].toObject();
            getUserByEmailResponse.token = await generateTokenAndLogin(getUserByEmailResponse, res);
            delete getUserByEmailResponse.password;
            return Utility.response(res, getUserByEmailResponse, AppConstant.MESSAGE.USER_REGISTERED_SUCCESSFULLY, AppConstant.SUCCESS);
        }

        user.location.coordinates = req.body.location;
        user.location.type = "Point";
        let registerUserResponse = await UserManager.registerUser(user);
        console.log(registerUserResponse);
        registerUserResponse = registerUserResponse.toObject();
        if (!registerUserResponse) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_REGISTER, AppConstant.FAILURE);
        }
        registerUserResponse.token = await generateTokenAndLogin(registerUserResponse, res);
        delete registerUserResponse.password;
        return Utility.response(res, registerUserResponse, AppConstant.MESSAGE.USER_REGISTERED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_REGISTER, AppConstant.FAILURE);
    }

};
exports.registerUser = async function (req, res) {
    try {
        const user = new User(req.body);
        console.log(req.body)
        console.log(user);
        const getUserByEmailResponse = await UserManager.getUserByEmailAndPhone(user);
        if (getUserByEmailResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.USER_ALREADY_EXISTS, AppConstant.CONFLICT);
        }

        user.password = bcrypt.hashSync(user.password, AppConstant.SALT.ROUNDS);
        user.location.coordinates = req.body.location;
        user.location.type = "Point";
        let registerUserResponse = await UserManager.registerUser(user);
        console.log(registerUserResponse);
        registerUserResponse = registerUserResponse.toObject();
        if (!registerUserResponse) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_REGISTER, AppConstant.FAILURE);
        }

        // registerUserResponse.token = await generateTokenAndLogin(registerUserResponse, res);
        // delete registerUserResponse.password;
        return Utility.response(res, {}, AppConstant.MESSAGE.USER_REGISTERED_SUCCESSFULLY, AppConstant.SUCCESS);
        // return Utility.response(res, registerUserResponse, AppConstant.MESSAGE.USER_REGISTERED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e)
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_REGISTER, AppConstant.FAILURE);
    }

};
exports.getUserByParams = async function (req, res) {
    try {
        let userResponse;
        let location = req.query.location;
        if (location) {
            location = location.split(',');
            location[0] = +location[0];
            location[1] = +location[1];
            userResponse = await UserManager.getUserByLocation(location, req.params.userId);
            return Utility.response(res, userResponse, AppConstant.MESSAGE.USERS_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);
        }
        userResponse = await UserManager.getUser();

        return Utility.response(res, userResponse, AppConstant.MESSAGE.USERS_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e);
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_GET_DATA, AppConstant.FAILURE);
    }

};
exports.getUserById = async function (req, res) {
    try {
        let userResponse;
        let userId = req.params.userId;
        userResponse = await UserManager.getPublicUserById(userId);
        if (!userResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.USER_NOT_REGISTERED, AppConstant.NOT_FOUND);

        }
        return Utility.response(res, userResponse[0], AppConstant.MESSAGE.USERS_FETCHED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        console.log(e);
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_GET_DATA, AppConstant.FAILURE);
    }

};

exports.addFeedback = async function (req, res) {
    try {
        const reqObj = {
            userId: req.body.userId,
            feedback: req.body.feedback,
        };
        let getUserByEmailResponse = await UserManager.getUserById(reqObj.userId);
        if (!getUserByEmailResponse.length) {
            return Utility.response(res, {}, AppConstant.MESSAGE.USER_NOT_REGISTERED, AppConstant.NOT_FOUND);
        }
        await Utility.sendMail({
            subject: 'eeho - Feedback Shared',
            body: `<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
<style>
    *{
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    html,body{
        background: #eeeeee;
        font-family: 'Open Sans', sans-serif, Helvetica, Arial;
    }
    img{
        max-width: 100%;
    }
    /* This is what it makes reponsive. Double check before you use it! */
    @media only screen and (max-width: 480px){
        table tr td{
            width: 100% !important;
            float: left;
        }
    }
</style>
</head>

<body style="background: #eeeeee; padding: 10px; font-family: 'Open Sans', sans-serif, Helvetica, Arial;">

<!-- ** Table begins here
----------------------------------->
<!-- Set table width to fixed width for Outlook(Outlook does not support max-width) -->
<table width="100%" cellpadding="0" cellspacing="0" bgcolor="FFFFFF" style="background: #ffffff; max-width: 600px !important; margin: 0 auto; background: #ffffff;">
    <tr>
        <td style="padding: 20px; text-align: center; background: #76ce3e;">
            <h1 style="color: #ffffff">${getUserByEmailResponse[0].name}, has shared a feedback</h1>
        </td>
    </tr>


    <tr>
        <td style="padding: 20px; text-align: center;">
            <!-- ** 100% width
            ----------------------------------->
            <p style="font-size:22px;">Feedback - ${reqObj.feedback}</p>
        </td>
    </tr>
    <tr>
        <td style="padding: 20px; background: #2B2E34;">

            <!-- ** 50% and 50%
            ----------------------------------->
            <table border="0" cellpadding="0" cellspacing="0" a>
                <tr>
                    <td width="50%" style="width: 50%; padding: 10px; color: #ffffff; text-align: left;" valign="top">
                        <h2>User's Address</h2>
                        <p style="font-size: 14px;">${getUserByEmailResponse[0].address}</p>
                    </td>
                    <td width="50%" style="width: 50%; padding: 10px; color: #ffffff; text-align: left;" valign="top">
                        <h2>Contact Information</h2>
                        <!-- ** Footer contact
                        ----------------------------------->
                        <table border="0" style="font-size: 14px;">
                            <tr><td>Phone: ${getUserByEmailResponse[0].phone}</td></tr>
                            <tr><td>Email:  ${getUserByEmailResponse[0].email}</td></tr>
                        </table>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>


<!-- ** Bottom Message
----------------------------------->
<p style="text-align: center; color: #666666; font-size: 12px; margin: 10px 0;">
    Copyright © 2020. All&nbsp;rights&nbsp;reserved.<br />
</p>

</center>

</body>
</html>`,
            email: AppConstant.CONTACT.EMAIL
        });
        return Utility.response(res, {}, AppConstant.MESSAGE.FEEDBACK_RAISED, AppConstant.SUCCESS);

    } catch (e) {
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_ADD_FEEDBACK, AppConstant.FAILURE);
    }

};

exports.addDeviceToken = async function (req, res) {
    try {

        const addTokenResponse = await UserManager.addToken({
            userId: req.body.userId,
            deviceToken: req.body.deviceToken
        });
        if (!addTokenResponse) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_ADD_TOKEN, AppConstant.FAILURE);

        }
        return Utility.response(res, {}, AppConstant.MESSAGE.TOKEN_ADDED_SUCCESSFULLY, AppConstant.SUCCESS);

    } catch (e) {
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_ADD_TOKEN, AppConstant.FAILURE);
    }

};

async function generateTokenAndLogin(userResponse, res) {
    try {
        const token = jwt.sign({
            id: userResponse._id,
            email: userResponse.email,
        }, AppConfig.SECRET);
        await UserManager.removeSession(userResponse._id.toString());

        const sessionObj = {
            userId: userResponse._id.toString(),
            token,
            addedOn: Date.now(),
            isLogin: 1,
        };
        const session = new Session(sessionObj);
        const saveSessionResponse = await UserManager.saveSession(session);
        if (!saveSessionResponse) {
            return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_CREATE_TOKEN, AppConstant.FAILURE);
        }
        return token;
    } catch (e) {
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_CREATE_TOKEN, AppConstant.FAILURE);
    }
}
