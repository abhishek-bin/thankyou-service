const Blog = require('../models/blog');

module.exports = {

    getBlog: blogId => Blog.getBlog(blogId),

    addBlog: blogObj => blogObj.addBlog(),

    likeBlog: blogObj => Blog.likeBlog(blogObj),

    commentBlog: blogObj => Blog.commentBlog(blogObj),

    unlikeBlog: blogObj => Blog.unlikeBlog(blogObj),

};
