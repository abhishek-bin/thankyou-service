const Donate = require('../models/donation');
const Message = require('../models/message');
const Notification = require('../models/notifications');

module.exports = {

    // getBlog: blogId => Blog.getBlog(blogId),
    getDonation: donationId => Donate.getDonation(donationId),

    updateDonation: donationId => Donate.updateDonation(donationId),

    getNotif: userId => Notification.getNotificationByUserId(userId),

    getNotifCount: userId => Notification.getNotifCount(userId),

    getMsgCount: userId => Message.getMsgCount(userId),

    updateNotif: notifId => Notification.updateNotif(notifId),

    addDonation: donateObj => donateObj.saveDonate(),

    addPayment: paymentObj => paymentObj.savePayment(),


};
