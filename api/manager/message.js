const Message = require('../models/message');

module.exports = {

    getMessage: reqObj => Message.getMessage(reqObj),

    getChatHistoryS: reqObj => Message.getChatHistorySender(reqObj),

    getChatHistoryR: reqObj => Message.getChatHistoryReceiver(reqObj),

    UnreadMessageCount: reqObj => Message.UnreadMessageCount(reqObj),

    markRead: reqObj => Message.markRead(reqObj),

    getEachUserChatCount: (idList, userId) => Message.getEachUserChatCount(idList, userId),

    saveMessage: reqObj => reqObj.saveMessage(),
};
