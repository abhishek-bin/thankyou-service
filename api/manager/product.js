const Product = require('../models/product');
const Rating = require('../models/rating');

module.exports = {

    getProduct: productObj => Product.getProduct(productObj),

    saveRating: ratingObj => ratingObj.saveRating(),

    getRating: ratingObj => Rating.getRating(ratingObj),

    getRatingByUserId: ratingObj => Rating.getRatingByUserId(ratingObj),

    getProductExist: productObj => Product.getProductExist(productObj),

    updateProduct: productObj => Product.updateProduct(productObj),

    getProductById: productObj => Product.getProductById(productObj),

    getProductByIdPlain: prodId => Product.getProductByIdPlain(prodId),

    getProductByUserId: userId => Product.getProductByUserId(userId),

    addProduct: productObj => productObj.addProduct(),

    saveNotif: notObj => notObj.saveNotification(),

    commentProduct: productObj => Product.commentProduct(productObj),

    likeProduct: productObj => Product.likeProduct(productObj),

    rateProduct: productObj => Product.rateProduct(productObj),

    getProductByCategory: productObj => Product.getProductByCategory(productObj),
    getProductByMe: productObj => Product.getProductByMe(productObj),

    unlikeProduct: productObj => Product.unlikeProduct(productObj),
};
