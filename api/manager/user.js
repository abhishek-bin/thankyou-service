const User = require('../models/user');
const Media = require('../models/media');
const Message = require('../models/message');
const Session = require('../models/session');
const Feedback = require('../models/feedback');

module.exports = {

    registerUser: userObj => userObj.saveUser(),

    getUserByEmailAndPhone: reqObj => User.getUserByEmailAndPhone(reqObj),

    getUserByEmail: reqObj => User.getUserByEmail(reqObj),

    getUserByPhone: reqObj => User.getUserByPhone(reqObj),

    getUser: () => User.getUser(),

    getActiveToken: (userObj) => User.getActiveToken(userObj),

    getUserById: (id) => User.getUserById(id),

    getPublicUserById: (id) => User.getPublicUserById(id),

    updateUser: (userObj) => User.updateUser(userObj),

    addToken: (userObj) => User.addToken(userObj),

    getUserByLocation: (location, userId) => User.getUserByLocation(location, userId),

    removeSession: userId => Session.removeSession(userId),

    saveSession: sessionObj => sessionObj.saveSession(),

    saveMedia: mediaObj => mediaObj.saveMedia(),

    getUserSession: reqObj => Session.getSession(reqObj),

    addFeedback: feedback => feedback.saveFeedback(),

    getChatUser: (reqObj) => User.getChatUser(reqObj),

    deleteChat: (reqObj) => Message.deleteChat(reqObj),


};
