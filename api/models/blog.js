const mongoose = require('mongoose');

const BlogSchema = new mongoose.Schema({

    title: {type: String, default: ''},
    mediaUrl: {type: String, default: ''},
    description: {type: String, default: ''},
    shortDescription: {type: String, default: ''},
    blogType: {type: String, default: ''},
    addedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    like: [{
        type: mongoose.Schema.Types.ObjectId, ref: 'User',
        addedOn: {type: Number, default: Date.now()},
    }],
    comment: [{
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        text: {type: String, default: ''},
        addedOn: {type: Number, default: Date.now()},
        isDeleted: {type: Number, default: 0},
    }],
    addedOn: {type: Number, default: Date.now()},
    modifiedOn: {type: Number, default: 0},
    deletedOn: {type: Number, default: 0},
    isDeleted: {type: Number, default: 0},
});

BlogSchema.method({
    addBlog() {
        return this.save();
    },
});

BlogSchema.static({
    getBlog(blogId) {
        if (blogId) {
            return this.aggregate([{
                $match: {
                    _id: mongoose.Types.ObjectId(blogId)
                }
            }, {
                $lookup: {
                    localField: 'comment.userId',
                    foreignField: '_id',
                    from: 'users',
                    as: 'commentMetaData'
                }
            }, {
                $lookup: {
                    localField: 'addedBy',
                    foreignField: '_id',
                    from: 'users',
                    as: 'addedBy'
                }
            },
                {
                    $unwind: '$addedBy'
                },
                {
                    $project:
                        {
                            title: 1,
                            description: 1,
                            blogType: 1,
                            like: 1,
                            profileUrl: '$addedBy.profileUrl',
                            // like: {$size: '$like'},
                            comment: 1,
                            // comment: {$size: '$comment'},
                            addedOn: 1,
                            modifiedOn: 1,
                            deletedOn: 1,
                            shortDescription:1,
                            isDeleted: 1,
                            mediaUrl: 1,
                            commentMetaData: {
                                name: 1,
                                profileUrl: 1,
                                _id: 1
                            }
                        }
                },
            ]);
        } else {
            return this.aggregate([{
                $match: {isDeleted: 0}
            },
                {
                    $lookup: {
                        localField: 'addedBy',
                        foreignField: '_id',
                        from: 'users',
                        as: 'addedBy'
                    }
                },
                {
                    $unwind: '$addedBy'
                },
                {
                    $project:
                        {
                            title: 1,
                            description: {$substr: ["$description", 0, 50]},
                            blogType: 1,
                            like: 1,
                            // like: {$size: '$like'},
                            comment: {$size: '$comment'},
                            addedOn: 1,
                            modifiedOn: 1,
                            mediaUrl: 1,
                            deletedOn: 1,
                            isDeleted: 1,
                            shortDescription:1,
                            profileUrl: '$addedBy.profileUrl',
                        }
                }, {
                    $sort: {addedOn: -1}
                }
            ]);
        }

    },
    likeBlog(reqObj) {
        return this.update({
            _id: reqObj.blogId
        }, {$addToSet: {like: reqObj.userId}});
    },
    unlikeBlog(reqObj) {
        console.log("hola")
        return this.update({
                _id: reqObj.blogId
            }, {$pull: {like: reqObj.userId}},
        );
    },
    commentBlog(reqObj) {
        return this.update({_id: reqObj.blogId}, {
            $push: {
                comment: {
                    userId: reqObj.userId,
                    text: reqObj.text
                }
            }
        });
    },
});

module.exports = mongoose.model('Blog', BlogSchema);
