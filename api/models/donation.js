const mongoose = require('mongoose');

const DonateSchema = new mongoose.Schema({

    addedBy: {type: mongoose.Schema.ObjectId, ref: 'User'},
    type: {type: String, default: ''}, // talent, health , education
    videoUrl: {type: String, default: ''},
    documentUrl: {type: String, default: ''},
    profileUrl: {type: String, default: ''},
    description: {type: String, default: ''},
    name: {type: String, default: ''},
    age: {type: Number, default: 0},
    targetAmount: {type: Number, default: 0},
    isDeleted: {type: Number, default: 0},
    deletedOn: {type: Number, default: 0},
    addedOn: {type: Number, default: 0},
});

DonateSchema.method({
    saveDonate() {
        return this.save();
    },
});

DonateSchema.static({
    getDonation(donationId = "") {
        if (donationId.length) {
            return this.aggregate([{
                $match: {
                    isDeleted: 0,
                    _id: mongoose.Types.ObjectId(donationId)
                }
            },
                {
                    $lookup: {
                        localField: '_id',
                        foreignField: 'donationId',
                        from: 'payments',
                        as: 'payment'
                    }
                },
                {
                    $project: {
                        name: 1,
                        age: 1,
                        raisedAmount: {$sum: "$payment.amount"},
                        description: 1,
                        targetAmount: 1,
                        addedBy: 1,
                        type: 1, // talent, health , education
                        videoUrl: 1,
                        documentUrl: 1,
                        profileUrl: 1,
                    }
                }
            ]);
        } else {
            return this.aggregate([{
                $match: {
                    isDeleted: 0
                }
            },
                {
                    $lookup: {
                        localField: '_id',
                        foreignField: 'donationId',
                        from: 'payments',
                        as: 'payment'
                    }
                },
                {
                    $project: {
                        name: 1,
                        age: 1,
                        raisedAmount: {$sum: "$payment.amount"},
                        description: 1,
                        targetAmount: 1,
                        addedBy: 1,
                        type: 1, // talent, health , education
                        videoUrl: 1,
                        documentUrl: 1,
                        profileUrl: 1,
                    }
                }
            ]);

        }
    },
    updateDonation(reqObj) {
        const donationId = reqObj.donationId;
        delete reqObj.donationId;
        return this.findOneAndUpdate({_id: donationId}, {
                $set: reqObj
            }, {new: true}
        );
    },

});
module.exports = mongoose.model('Donate', DonateSchema);
