const mongoose = require('mongoose');

const FeedbackSchema = new mongoose.Schema({

    subject: { type: String, default: '' },
    message: { type: String, default: '' },
    userId: { type: String, default: '' },
    addedOn: { type: Number, default: Date.now() }
});

FeedbackSchema.method({
    saveFeedback() {
        return this.save();
    },
});

FeedbackSchema.static({
 getFeedback(reqObj) {
        return this.find(reqObj);
    },
});

module.exports = mongoose.model('Feedback', FeedbackSchema);
