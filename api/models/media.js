const mongoose = require('mongoose');

const MediaSchema = new mongoose.Schema({

    mediaUrl: { type: String, default: '' },
    name: { type: String, default: '' },
    type: { type: String, default: '' },
    Etag: { type: String, default: '' },
    addedOn: { type: Number, default: Date.now() },
    modifiedOn: { type: Number, default: 0 },
    deletedOn: { type: Number, default: 0 },
    isDeleted: { type: Number, default: 0 },
});

MediaSchema.method({
    saveMedia() {
        return this.save();
    },
});
//
// MediaSchema.static({
// });

module.exports = mongoose.model('Media', MediaSchema);
