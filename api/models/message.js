const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({

    message: {type: String, default: ''},
    senderId: {type: mongoose.Schema.ObjectId, ref: 'User'},
    receiverId: {type: mongoose.Schema.ObjectId, ref: 'User'},
    addedOn: {type: Number, default: Date.now()},
    isRead: {type: Number, default: 0},
    deletedOn: {type: Number, default: 0},
    isDeleted: {type: Number, default: 0},
    deletedBy: [{type: String, default: null}]
});
MessageSchema.index({senderId: 1});
MessageSchema.index({receiverId: 1});
MessageSchema.index({addedOn: 1});
MessageSchema.method({
    saveMessage() {
        return this.save();
    },
});

MessageSchema.static({
    getMessage(reqObj) {
        return this.aggregate([{
            $match: {
                $or: [
                    {
                        $and: [
                            {
                                senderId: mongoose.Types.ObjectId(reqObj.receiverId)
                            },
                            {
                                receiverId: mongoose.Types.ObjectId(reqObj.senderId)
                            },
                            {
                                deletedBy: {$ne: null}
                            }, {
                                deletedBy: {$ne: reqObj.senderId}
                            }

                        ]
                    }, {
                        $and: [{
                            senderId: mongoose.Types.ObjectId(reqObj.senderId)
                        },
                            {receiverId: mongoose.Types.ObjectId(reqObj.receiverId)},
                            {
                                deletedBy: {$ne: null}
                            }, {
                                deletedBy: {$ne: reqObj.senderId}
                            }
                        ]
                    }
                ]
            }
        }, {
            $match: {
                addedOn: {$gt: +reqObj.epoch}
            }
        },
            {
                $sort: {
                    addedOn: 1
                }
            },])
    },
    getMsgCount(userId) {
        return this.find({
                $and: [{
                    isRead: 0
                }, {
                    receiverId: userId
                }, {
                    deletedBy: {$ne: null}
                }, {
                    deletedBy: {$ne: userId}
                }]
            }
        ).count();
    },
    getChatHistorySender(reqObj) {
        return this.aggregate([{
            $match: {
                $and: [{
                    senderId: mongoose.Types.ObjectId(reqObj.userId)
                }, {
                    deletedBy: {$ne: null}
                }, {
                    deletedBy: {$ne: reqObj.userId}
                }]
            }
        },
            {
                $lookup: {
                    localField: 'receiverId',
                    foreignField: '_id',
                    from: 'users',
                    as: 'receiver'
                }
            },
            {
                $unwind: '$receiver'
            },
            {$group: {_id: "$receiver", addedOn: { $push: "$addedOn" }}}
        ])
    },
    getChatHistoryReceiver(reqObj) {
        return this.aggregate([{
            $match: {
                $and: [{receiverId: mongoose.Types.ObjectId(reqObj.userId)}, {deletedBy: {$ne: null}}, {deletedBy: {$ne: reqObj.userId}}]
            }
        }, {
            $lookup: {
                localField: 'senderId',
                foreignField: '_id',
                from: 'users',
                as: 'sender'
            }
        },
            {
                $unwind: '$sender'
            },
            {$group: {_id: "$sender", addedOn: { $push: "$addedOn" }}}
        ])
    },
    getEachUserChatCount(idList, userId) {
        return this.find({
            isRead: 0,
            senderId: {$in: idList},
            receiverId: userId
        })
    },
    UnreadMessageCount(obj) {
        return this.find({
            isRead: 0,
            senderId: obj._id,
            receiverId: obj.userId,
            deletedBy: {$ne: obj.userId}
        }).count()
    },

    markRead(obj) {
        console.log(obj)
        return this.update({senderId: obj.senderId, receiverId: obj.receiverId, isRead: 0}, {
            $set: {
                isRead: 1
            }
        }, {multi: true});
    },
    deleteChat(obj) {
        console.log(obj)
        return this.update({
                $or: [{
                    senderId: obj.userId,
                    receiverId: {$in: obj.receiverList}
                },
                    {
                        senderId: {$in: obj.receiverList},
                        receiverId: obj.userId
                    }]
            }, {$addToSet: {deletedBy: obj.userId}},
            {multi: true});
    },
});

module.exports = mongoose.model('Message', MessageSchema);
