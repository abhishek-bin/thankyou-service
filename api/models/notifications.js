const mongoose = require('mongoose');

const NotificationSchema = new mongoose.Schema({

    receiverId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    body: {type: String, default: ''},
    title: {type: String, default: ''},
    addedOn: {type: Number, default: 0},
    isRead: {type: Number, default: 0},
    data: {type: String, default: ''}
});

NotificationSchema.method({
    saveNotification() {
        console.log("notif saved!")
        return this.save();
    },
});

NotificationSchema.static({
    getNotificationByUserId(userId) {
        return this.find({receiverId: userId}).sort({addedOn: -1});
    },
    getNotifCount(userId) {
        return this.find({receiverId: userId, isRead: 0}).count();
    },
    updateNotif(userId) {
        return this.findOneAndUpdate({_id: userId}, {
            $set: {
                isRead: 1
            }, new: true
        });
    },
});

module.exports = mongoose.model('Notification', NotificationSchema);
