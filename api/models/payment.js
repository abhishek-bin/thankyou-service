const mongoose = require('mongoose');

const PaymentSchema = new mongoose.Schema({

    donor: {type: mongoose.Schema.ObjectId, ref: 'User'},
    donationId: {type: mongoose.Schema.ObjectId, ref: 'Donation'},
    amount: {type: Number, default: 0},
    paymentId: {type: String, default: ''},
    status: {type: String, default: ''},
    addedOn: {type: Number, default: 0},
});

PaymentSchema.method({
    savePayment() {
        return this.save();
    },
});

PaymentSchema.static({});

module.exports = mongoose.model('Payment', PaymentSchema);
