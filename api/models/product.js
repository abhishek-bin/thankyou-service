const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new mongoose.Schema({

    price: {type: Number, default: 0},
    to: {type: String, default: ""},
    from: {type: String, default: ""},
    title: {type: String, default: ""},
    description: {type: String, default: ""},
    type: {type: String, default: ""}, // food or non-food
    status: {type: String, default: "in-progress"}, // done, cancelled
    category: {type: String, default: ''},
    addedOn: {type: Number, default: Date.now()},
    expiryTime: {type: String, default: ''},
    addedBy: {type: Schema.Types.ObjectId, ref: 'User'},
    isApproved: {type: Number, default: 0},
    mediaUrl: {type: String, default: ''},
    isDonated: {type: Number, default: 0},
    comment: [{
        userId: {type: Schema.Types.ObjectId, ref: 'User'},
        text: {type: String, default: ''},
        addedOn: {type: Number, default: Date.now()},
        isDeleted: {type: Number, default: 0},
    }],
    like: [{
        type: Schema.Types.ObjectId, ref: 'User'
    }],
    location: {
        type: {
            type: String,
            enum: ['Point'],
            required: false
        },
        coordinates: {
            type: [Number],
            required: false
        }
    },
    modifiedOn: {type: Number, default: 0},

});
ProductSchema.index({location: '2dsphere'});

ProductSchema.method({
    addProduct() {
        return this.save();
    },
});

ProductSchema.static({

    getProductByIdPlain(prodId) {
        return this.find({_id: prodId});
    },
    getProduct(reqObj) {
        return this.aggregate([
            {
                $geoNear: {
                    near: {type: "Point", coordinates: reqObj.location},
                    distanceField: "distance",
                    minDistance: 0,
                    maxDistance: 1000000,
                    spherical: true,
                    key: 'location'
                }
            },
            {
                $match: {
                    addedBy: {"$nin": [mongoose.Types.ObjectId(reqObj.userId)]},
                    status: 'in-progress'
                }
            },
            {
                $lookup: {
                    from: 'ratings',
                    localField: '_id',
                    foreignField: 'productId',
                    as: 'rating'
                }
            }, {
                $lookup: {
                    from: 'users',
                    localField: 'addedBy',
                    foreignField: '_id',
                    as: 'addedBy'
                }
            },
            {
                $unwind: '$addedBy'
            },
            {
                $project: {
                    addedOn: 1,
                    category: 1,
                    description: 1,
                    deviceToken: '$addedBy.deviceToken',
                    expiryTime: 1,
                    isApproved: 1,
                    isDonated: 1,
                    location: 1,
                    mediaUrl: 1,
                    to: 1,
                    from: 1,
                    modifiedOn: 1,
                    price: 1,
                    status: 1,
                    title: 1,
                    like: 1,
                    comment: 1,
                    type: 1,
                    distance: 1,
                    profileUrl: '$addedBy.profileUrl',
                    name: '$addedBy.name',
                    rating: {$avg: "$rating.value"}
                }
            }
        ]);
    },
    getProductExist() {
        return this.find({}).limit(2);
    },
    getProductById(productId) {
        return this.aggregate([
            {
                $match: {_id: mongoose.Types.ObjectId(productId)}
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'addedBy',
                    foreignField: '_id',
                    as: 'addedBy'
                }
            },
            {
                $unwind: '$addedBy'
            },
            {
                $lookup: {
                    from: 'ratings',
                    localField: '_id',
                    foreignField: 'productId',
                    as: 'rating'
                }
            },
            {
                $project: {
                    addedBy: '$addedBy._id',
                    addedOn: 1,
                    category: 1,
                    description: 1,
                    deviceToken: '$addedBy.deviceToken',
                    expiryTime: 1,
                    isApproved: 1,
                    isDonated: 1,
                    location: 1,
                    mediaUrl: 1,
                    to: 1,
                    from: 1,
                    modifiedOn: 1,
                    price: 1,
                    status: 1,
                    title: 1,
                    like: 1,
                    comment: 1,
                    type: 1,
                    userId: '$addedBy._id',
                    profileUrl: '$addedBy.profileUrl',
                    name: '$addedBy.name',
                    email: '$addedBy.email',
                    phone: '$addedBy.phone',
                    address: '$addedBy.address',
                    zipCode: '$addedBy.zipCode',
                    rating: {$avg: "$rating.value"}
                }
            }
        ]);
    },
    getProductByUserId(userId) {
        return this.aggregate([
            {
                $match: {addedBy: mongoose.Types.ObjectId(userId)}
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'addedBy',
                    foreignField: '_id',
                    as: 'addedBy'
                }
            },
            {
                $unwind: '$addedBy'
            }, {
                $lookup: {
                    from: 'ratings',
                    localField: '_id',
                    foreignField: 'productId',
                    as: 'rating'
                }
            }, {
                $project: {
                    addedOn: 1,
                    addedBy: '$addedBy._id',
                    category: 1,
                    description: 1,
                    expiryTime: 1,
                    isApproved: 1,
                    isDonated: 1,
                    location: 1,
                    mediaUrl: 1,
                    to: 1,
                    from: 1,
                    modifiedOn: 1,
                    price: 1,
                    status: 1,
                    title: 1,
                    type: 1,
                    profileUrl: '$addedBy.profileUrl',
                    name: '$addedBy.name',
                    rating: {$avg: "$rating.value"}

                }
            }
        ]);
    },
    getProductByMe(reqObj) {
        if (reqObj.productId === 'product') {
            return this.find({
                addedBy: reqObj.userId,
                status: 'in-progress'
            }, {title: 1, mediaUrl: 1}).sort({addedOn: -1}).limit(5);
        } else {
            return this.find({
                addedBy: reqObj.userId,
                status: 'in-progress',
                _id: {$ne: reqObj.productId}
            }, {title: 1, mediaUrl: 1}).sort({addedOn: -1}).limit(5);
        }

    },
    getProductByCategory(reqObj) {
        return this.find({
            addedBy: {$ne: reqObj.userId},
            _id: {$ne: reqObj.productId},
            status: 'in-progress',
            category: reqObj.category
        }, {title: 1, mediaUrl: 1}).sort({addedOn: -1}).limit(5);
    },
    commentProduct(reqObj) {
        return this.update({_id: reqObj.productId}, {
            $push: {
                comment: {
                    userId: reqObj.userId,
                    text: reqObj.text
                }
            }
        });
    },
    likeProduct(reqObj) {
        return this.update({
            _id: reqObj.productId
        }, {$addToSet: {like: reqObj.userId}});
    },
    unlikeProduct(reqObj) {
        return this.update({
                _id: reqObj.productId
            }, {$pull: {like: reqObj.userId}},
        );
    },
    updateProduct(reqObj) {
        const prodId = reqObj.productId;
        delete reqObj.productId;
        return this.findOneAndUpdate({
            _id: prodId
        }, {$set: reqObj});
    },


});

module.exports = mongoose.model('Product', ProductSchema);
