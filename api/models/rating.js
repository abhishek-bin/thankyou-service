const mongoose = require('mongoose');

const RatingSchema = new mongoose.Schema({

    userId: {type: mongoose.Schema.ObjectId, ref: 'User'},
    productId: {type: mongoose.Schema.ObjectId, ref: 'Product'},
    value: {type: Number, default: 0},
    addedOn: {type: Number, default: 0},
});

RatingSchema.method({
    saveRating() {
        return this.save();
    },
});

RatingSchema.static({
    getRating(obj) {
        return this.find({
            productId: obj.productId,
            userId: obj.userId
        });
    },
    getRatingByUserId(obj) {
        return this.find({
            productId: {$in: obj.productIdList}
        });
    },
});

module.exports = mongoose.model('Rating', RatingSchema);
