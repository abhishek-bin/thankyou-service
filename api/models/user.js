'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {type: String, default: ''},
    dob: {type: String, default: ''},
    password: {type: String, default: ''},
    email: {type: String, default: ''},
    phone: {type: String, default: ''},
    fbId: {type: String, default: ''},
    gmailId: {type: String, default: ''},
    deviceToken: {type: String, default: ''},
    otp: {type: String, default: ''},
    isVerified: {type: Number, default: 0},
    radius: {type: Number, default: 100000},
    role: {type: Number, default: 2},
    addedOn: {type: Date, default: Date.now},
    address: {type: String, default: ''},
    profileUrl: {type: String, default: ''},
    zipCode: {type: String, default: ''},
    isDeleted: {type: Number, default: 0},
    location: {
        type: {
            type: String,
            enum: ['Point'],
            required: false
        },
        coordinates: {
            type: [Number],
            required: false
        }
    },
});
UserSchema.method({
    saveUser() {
        return this.save();
    },
});
UserSchema.index({email: 1});
UserSchema.index({location: '2dsphere'});

UserSchema.static({

    getUserByEmailAndPhone(reqObj) {
        return this.find({$or: [{email: reqObj.email}, {phone: reqObj.phone}]});
    },

    getUserByEmail(reqObj) {
        return this.find({email: reqObj.email, isDeleted: 0});
    },

    getUserByPhone(reqObj) {
        return this.find({phone: reqObj.phone, isDeleted: 0});
    },

    getUser() {
        return this.find({isDeleted: 0, role: 2});
    },

    updateUser(reqObj) {
        const userId = reqObj.userId;
        delete reqObj.userId;
        return this.findOneAndUpdate({_id: userId}, {
                $set: reqObj
            }, {new: true}
        );
    },
    addToken(reqObj) {
        return this.findOneAndUpdate({_id: reqObj.userId}, {
                $set: {
                    deviceToken: reqObj.deviceToken
                }
            }, {new: true}
        );
    },
    getUserById(id) {
        return this.find({
            _id: id,
            isDeleted: 0
        });
    },
    getPublicUserById(id) {
        return this.find({
            _id: id,
            isDeleted: 0
        }, {name: 1, profileUrl: 1, email: 1, phone: 1});
    },

    getUserByLocation(location, userId) {
        console.log("hum")
        return this.aggregate([

            {
                $geoNear: {
                    near: {type: "Point", coordinates: location},
                    distanceField: "distance",
                    minDistance: 0,
                    maxDistance: 1000000,
                    spherical: true
                }
            }, {
                $match: {
                    isDeleted: 0,
                    role: 2,
                    // isVerified: 1,
                    _id: {$ne: mongoose.Types.ObjectId(userId)}
                }
            },
        ]);
    },
    getChatUser(reqObj) {
        return this.aggregate([
            {
                $match: {
                    isDeleted: 0,
                    role: 2,
                    _id: {$ne: mongoose.Types.ObjectId(reqObj.userId)}
                }
            },
            {
                $project: {
                    name: 1,
                    profileUrl: 1
                }
            }
        ]);
    },
    getActiveToken(reqObj) {
        return this.aggregate([
            {
                $geoNear: {
                    near: {type: "Point", coordinates: reqObj.location},
                    distanceField: "distance",
                    minDistance: 0,
                    maxDistance: 9999999,
                    spherical: true
                }
            },
            {
                $match: {
                    isDeleted: 0,
                    role: 2,
                    _id: {$ne: mongoose.Types.ObjectId(reqObj.userId)},
                    deviceToken: {$ne: ''},
                    $expr: { $lte: [ "$distance" , "$radius" ] }
                }
            },
            {
                $project: {
                    deviceToken: 1,
                    _id: 1
                }
            }
        ]);
    },
});

module.exports = mongoose.model('User', UserSchema);
