const User = require('../controllers/user');
const Donation = require('../controllers/donate');
const Product = require('../controllers/product');
const Message = require('../controllers/message');
const Blog = require('../controllers/blog');
const Router = require('express').Router();
const Validator = require('../../middlewere/validation');
const Authorize = require('../../middlewere/auth');
const Upload = require('../../utility/upload');

Router.get('/', (req, res, next) => {
    res.status(200).send('Its working fine');
});


Router.post('/login', Validator.login, User.login);

Router.post('/logout/:userId', User.logout);

Router.post('/register', Validator.register, User.registerUser);

Router.post('/fb-login', Validator.fbLogin, User.fbLogin);

Router.post('/gmail-login', Validator.gmailLogin, User.gmailLogin);

Router.post('/product', Authorize.authorizeUserRequest, Validator.addProduct, Product.addProduct);

Router.post('/donate', Authorize.authorizeUserRequest, Validator.donate, Product.donate);

Router.get('/test-location', Product.test);

Router.post('/want', Authorize.authorizeUserRequest, Validator.want, Product.want);

Router.post('/device-token', Validator.deviceToken, User.addDeviceToken);

Router.post('/product/user', Authorize.authorizeUserRequest, Product.getProduct);

Router.get('/product/:productId', Authorize.authorizeUserRequest, Product.getProductById);

Router.put('/request-listing/:productId', Authorize.authorizeUserRequest, Product.requestProduct);

Router.put('/product/:productId', Product.updateProductStatus);

Router.get('/my-product/:userId', Authorize.authorizeUserRequest, Product.getMyProductByUserId);

Router.post('/upload-media', Upload.uploadMedia);

Router.get('/user/:userId', Authorize.authorizeUserRequest, User.getUserByParams);

Router.get('/public-user/:userId', Authorize.authorizeUserRequest, User.getUserById);

Router.put('/like-product/:productId', Authorize.authorizeUserRequest, Product.likeProduct);

Router.put('/like-blog/:blogId', Authorize.authorizeUserRequest, Blog.likeBlog);

Router.post('/comment', Authorize.authorizeUserRequest, Validator.commentProduct, Blog.commentBlog);

Router.post('/blog', Validator.addBlog, Blog.addBlog);

Router.get('/blog', Authorize.authorizeUserRequest, Blog.getBlog);

Router.get('/chat-user/:userId', Authorize.authorizeUserRequest, Message.getChatUser);

Router.post('/chat', Authorize.authorizeUserRequest, Message.deleteChat);

Router.get('/message/:senderId/:receiverId/:epoch', Authorize.authorizeUserRequest, Message.getChatMessage);

Router.post('/message', Authorize.authorizeUserRequest, Validator.setChatMessage, Message.setChatMessage);

Router.put('/user', Authorize.authorizeUserRequest, Validator.updateUser, User.updateUser);

Router.get('/send-otp/:phone', User.sendOtp);

Router.get('/reset-otp/:phone', User.resetOtp);

Router.post('/verify-otp', Validator.verifyOtp, User.verifyOtp);

Router.post('/reset-password', Validator.resetPassword, User.resetPassword);

Router.post('/need-help', Authorize.authorizeUserRequest, Validator.needHelp, User.needHelp);

Router.post('/feedback', Authorize.authorizeUserRequest, Validator.feedback, User.addFeedback);

Router.post('/donation', Authorize.authorizeUserRequest, Validator.donation, Donation.addDonation);

Router.get('/donation', Authorize.authorizeUserRequest, Donation.getDonation);

Router.post('/payment-status', Authorize.authorizeUserRequest, Validator.payment, Donation.paymentStatus);

Router.delete('/donation/:donationId', Authorize.authorizeUserRequest, Donation.deleteDonation);

Router.get('/notification/:userId', Authorize.authorizeUserRequest, Donation.getNotification);

Router.get('/notificationCount/:userId', Authorize.authorizeUserRequest, Donation.getNotificationCnt);

Router.get('/chat-history/:userId', Authorize.authorizeUserRequest, Message.getChatHistory);

Router.put('/mark-read-message/:senderId', Authorize.authorizeUserRequest, Message.markReadMessage);

Router.put('/notification/:notificationId', Authorize.authorizeUserRequest, Donation.markReadNotification);

Router.get('/rate/:productId/:value', Authorize.authorizeUserRequest, Product.rateProduct);

Router.get('/related/:category/:productId', Authorize.authorizeUserRequest, Product.relatedProduct);

Router.get('/posted-product/:addedBy/:productId', Authorize.authorizeUserRequest, Product.productByMe);


module.exports = Router;
