const express = require('express');
const bodyParser = require('body-parser');
const config = require('./configs/config');
const dbConfig = require('./configs/db.js');
const Morgan = require('morgan');
const routes = require('./api/routes/routes'); //importing route
const expressValidator = require('express-validator');
const cors = require('cors');
const app = express();
const admin = require('firebase-admin');
const serviceAccount = require("./configs/eeho-e5e84-firebase-adminsdk-ouamd-e83a791252.json");

global.admin = admin;
app.use(cors());

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://eeho-dev.firebaseio.com"
});
app.use(Morgan("dev"));
// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(expressValidator());

//Create db connection
dbConfig();

//CORS Middleware

app.use('/api', routes); //register the route

app.listen(AppConfig.PORT, () => {
    console.log(`Eeho server is running on port ${AppConfig.PORT}`)
});
