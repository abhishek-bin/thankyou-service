const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
module.exports = function dbConfig(app) {
    const connector = mongoose.connect(AppConfig.DATABASE_URL, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        // retry to connect for 60 times
        reconnectTries: 60,
        // wait 1 second before retrying
        reconnectInterval: 1000,
        // ensureIndex: true
    }).then(res => {
        console.log("------Hi I am cloudDB. Ready to serve you-------");
    }, err => {
        console.log(err)
        console.log("------Oops you DB connection got some trouble------");
    })
}
