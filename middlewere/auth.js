/* eslint-disable max-len */
const jwt = require('jsonwebtoken');
const AppConstant = require('../utility/constant');
const Utility = require('../utility/util');
const UserManager = require('../api/manager/user');

module.exports = {

    authorizeUserRequest: async (req, res, next) => {
        let errors;
        req.checkHeaders('x-access-token', 'x-access-token cannot be empty').notEmpty();
        errors = req.validationErrors();
        if (errors) {
            Utility.response(res, {}, errors, AppConstant.UNPROCESSABLE_ENTITY);
        } else {
            const token = req.headers['x-access-token'];
            // console.log(token)
            try {
                const decoded = jwt.verify(token, AppConfig.SECRET);
                // console.log(decoded)
                const reqObj = { userId: decoded.id, token: token, isLogin: 1 };

                const sessionDetails = await UserManager.getUserSession(reqObj);

                if (!sessionDetails.length) {
                    return Utility.response(res, {}, AppConstant.MESSAGE.NOT_ALLOWED, AppConstant.FORBIDDEN);
                }
                const decodedSavedToken = jwt.verify(token, AppConfig.SECRET);

                if (decodedSavedToken.email !== decoded.email) {
                    return Utility.response(res, {}, AppConstant.MESSAGE.NOT_ALLOWED, AppConstant.FORBIDDEN);
                }
                if (decodedSavedToken.role !== decoded.role) {
                    return Utility.response(res, {}, AppConstant.MESSAGE.NOT_ALLOWED, AppConstant.FORBIDDEN);
                }
                /* passing decoded user object to the request object */
                req.user = decoded;
                // console.log(decoded);
                next();
            } catch (e) {
                return Utility.response(res, {}, AppConstant.MESSAGE.INVALID_TOKEN, AppConstant.FORBIDDEN);
            }
        }
    },
};
