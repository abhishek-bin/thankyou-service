const Utility = require('../utility/util');
const Constant = require('../utility/constant');

module.exports = {
    fbLogin: (req, res, next) => {
        let errors;
        req.check('name', 'name cannot be empty').notEmpty().isString();
        req.check('email', 'email cannot be empty').notEmpty().isString();
        req.check('location', 'location cannot be empty').notEmpty().isArray();
        req.check('fbId', 'fbId cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {
            req.body.name = req.body.name.replace(/(\w)(\w*)/g,
                (g0, g1, g2) => {
                    return g1.toUpperCase() + g2.toLowerCase();
                });
            req.body.email = req.body.email.toLowerCase();
            // req.body.city = req.body.city.toLowerCase();
            // req.body.country = req.body.country.toLowerCase();
            // req.body.state = req.body.state.toLowerCase();

            next();
        }
    },
    gmailLogin: (req, res, next) => {
        let errors;
        req.check('name', 'name cannot be empty').notEmpty().isString();
        req.check('email', 'email cannot be empty').notEmpty().isString();
        req.check('location', 'location cannot be empty').notEmpty().isArray();
        req.check('gmailId', 'gmailId cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {
            req.body.name = req.body.name.replace(/(\w)(\w*)/g,
                (g0, g1, g2) => {
                    return g1.toUpperCase() + g2.toLowerCase();
                });
            req.body.email = req.body.email.toLowerCase();
            // req.body.city = req.body.city.toLowerCase();
            // req.body.country = req.body.country.toLowerCase();
            // req.body.state = req.body.state.toLowerCase();

            next();
        }
    },
    register: (req, res, next) => {
        let errors;
        req.check('name', 'name cannot be empty').notEmpty().isString();
        req.check('dob', 'dob cannot be empty').notEmpty().isString();
        req.check('email', 'email cannot be empty').notEmpty().isString();
        req.check('password', 'password cannot be empty').notEmpty().isString();
        req.check('phone', 'phone cannot be empty').notEmpty().isString();
        req.check('address', 'address cannot be empty').notEmpty().isString();
        req.check('location', 'location cannot be empty').notEmpty().isArray();
        req.check('zipCode', 'zipCode cannot be empty').notEmpty();
        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {
            req.body.name = req.body.name.replace(/(\w)(\w*)/g,
                (g0, g1, g2) => {
                    return g1.toUpperCase() + g2.toLowerCase();
                });
            req.body.email = req.body.email.toLowerCase();
            // req.body.city = req.body.city.toLowerCase();
            // req.body.country = req.body.country.toLowerCase();
            // req.body.state = req.body.state.toLowerCase();

            next();
        }
    },
    login: (req, res, next) => {
        let errors;
        req.check('email', 'email cannot be empty').notEmpty();
        req.check('password', 'password cannot be empty').notEmpty();
        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    addProduct: (req, res, next) => {
        let errors;
        req.check('title', 'title cannot be empty').notEmpty();
        req.check('description', 'description cannot be empty').notEmpty();
        req.check('type', 'type cannot be empty').notEmpty();
        req.check('location', 'location cannot be empty').notEmpty();
        req.check('mediaUrl', 'mediaUrl cannot be empty').notEmpty();
        req.check('expiryTime', 'expiryTime cannot be empty').notEmpty();
        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    donate: (req, res, next) => {
        let errors;
        req.check('title', 'title cannot be empty').notEmpty();
        req.check('location', 'location cannot be empty').notEmpty();
        req.check('description', 'description cannot be empty').notEmpty();
        req.check('category', 'category cannot be empty').notEmpty();
        req.check('addedBy', 'addedBy cannot be empty').notEmpty();
        req.check('type', 'type cannot be empty').notEmpty();
        req.check('location', 'location cannot be empty').notEmpty();
        // req.check('price', 'price cannot be empty').notEmpty();
        req.check('mediaUrl', 'mediaUrl cannot be empty').notEmpty();
        // req.check('expiryTime', 'expiryTime cannot be empty').notEmpty();
        if (req.body.type && req.body.type === 'food')
            req.check('expiryTime', 'expiryTime cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    want: (req, res, next) => {
        let errors;
        req.check('category', 'category cannot be empty').notEmpty();
        req.check('title', 'title cannot be empty').notEmpty();
        req.check('location', 'location cannot be empty').notEmpty();
        req.check('description', 'description cannot be empty').notEmpty();
        req.check('type', 'type cannot be empty').notEmpty();
        req.check('addedBy', 'addedBy cannot be empty').notEmpty();
        // req.check('mediaUrl', 'mediaUrl cannot be empty').notEmpty();
        if (req.body.type && req.body.type === 'food')
            req.check('expiryTime', 'expiryTime cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    deviceToken: (req, res, next) => {
        let errors;
        req.check('userId', 'userId cannot be empty').notEmpty();
        req.check('deviceToken', 'deviceToken cannot be empty').notEmpty();
        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    commentProduct: (req, res, next) => {
        let errors;
        req.check('text', 'text cannot be empty').notEmpty();
        req.check('userId', 'userId cannot be empty').notEmpty();
        req.check('blogId', 'blogId cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    addFeedback: (req, res, next) => {
        let errors;
        req.check('subject', 'subject cannot be empty').notEmpty();
        req.check('userId', 'userId cannot be empty').notEmpty();
        req.check('message', 'message cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    setChatMessage: (req, res, next) => {
        let errors;
        req.check('message', 'message cannot be empty').notEmpty();
        req.check('senderId', 'senderId cannot be empty').notEmpty();
        req.check('receiverId', 'receiverId cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    updateUser: (req, res, next) => {
        let errors;
        req.check('userId', 'userId cannot be empty').notEmpty();
        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },

    likeProduct: (req, res, next) => {
        let errors;
        req.check('userId', 'userId cannot be empty').notEmpty();
        req.check('productId', 'productId cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    verifyOtp: (req, res, next) => {
        let errors;
        req.check('otp', 'otp cannot be empty').notEmpty();
        req.check('phone', 'phone cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    resetPassword: (req, res, next) => {
        let errors;
        req.check('password', 'password cannot be empty').notEmpty();
        req.check('phone', 'phone cannot be empty').notEmpty();
        req.check('otp', 'otp cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    feedback: (req, res, next) => {
        let errors;
        req.check('userId', 'userId cannot be empty').notEmpty();
        req.check('feedback', 'feedback cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    needHelp: (req, res, next) => {
        let errors;
        req.check('userId', 'userId cannot be empty').notEmpty();
        req.check('subject', 'subject cannot be empty').notEmpty();
        req.check('description', 'description cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    addBlog: (req, res, next) => {
        let errors;
        req.check('title', 'title cannot be empty').notEmpty();
        req.check('description', 'description cannot be empty').notEmpty();
        req.check('shortDescription', 'shortDescription cannot be empty').notEmpty();
        req.check('blogType', 'blogType cannot be empty').notEmpty();
        req.check('addedBy', 'addedBy cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    donation: (req, res, next) => {
        let errors;
        req.check('name', 'name cannot be empty').notEmpty();
        req.check('age', 'age cannot be empty').notEmpty();
        req.check('addedBy', 'addedBy cannot be empty').notEmpty();
        req.check('description', 'description cannot be empty').notEmpty();
        req.check('type', 'type cannot be empty').notEmpty().isIn(['talent', 'health', 'education'])
        req.check('videoUrl', 'videoUrl cannot be empty').notEmpty();
        req.check('profileUrl', 'profileUrl cannot be empty').notEmpty();
        req.check('targetAmount', 'targetAmount cannot be empty').notEmpty();

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
    payment: (req, res, next) => {
        let errors;
        req.check('donor', 'name cannot be empty').notEmpty();
        req.check('donationId', 'age cannot be empty').notEmpty();
        req.check('amount', 'addedBy cannot be empty').notEmpty();
        req.check('status', 'description cannot be empty').notEmpty().isIn(['success', 'failure']);

        errors = req.validationErrors();


        if (errors) {
            Utility.response(res, {}, errors, Constant.UNPROCESSABLE_ENTITY);
        } else {

            next();
        }
    },
};
