const AppConstant = require('./constant');
const Utility = require('./util');
const formidable = require('formidable');
const UserManager = require('../api/manager/user');
const Media = require('../api/models/media');
const fs = require('fs');
const AWS = require('aws-sdk');

module.exports.uploadMedia = async (req, res) => {
    console.log(req.body)
    const form = new formidable.IncomingForm();
    try {
        form.parse(req, (err, fields, files) => {
            console.log(files)
            console.log(fields)

            if (!files.file) {
                return Utility.response(res, {}, AppConstant.MESSAGE.NO_FILE_WAS_PROVIDED, AppConstant.UNPROCESSABLE_ENTITY);
            }
            if (files.file.size > AppConstant.MEDIA.MAX_FILE_SIZE) {
                return Utility.response(res, {}, AppConstant.MESSAGE.FILE_SIZE_TOO_LARGE, AppConstant.PAYLOAD_TOO_LARGE);
            }
            if (files.file.type.indexOf(AppConstant.MEDIA.TYPE) < 0) {
                return Utility.response(res, {}, AppConstant.MESSAGE.UNSUPPORTED_MEDIA_TYPE, AppConstant.UNSUPPORTED_MEDIA);
            }
            const BUCKET_NAME = AppConfig.BUCKET_NAME;
            const IAM_USER_KEY = AppConfig.USER_KEY;
            const IAM_USER_SECRET = AppConfig.USER_SECRET;

            AWS.config.update({
                accessKeyId: IAM_USER_KEY,
                secretAccessKey: IAM_USER_SECRET,
                region: "us-east-2"
            });
            const s3bucket = new AWS.S3();
            fs.readFile(files.file.path, (err, fileBuffer) => {
                const fileName = Date.now().toString() + files.file.name.substring(files.file.name.lastIndexOf('.'));
                const params = {
                    Bucket: BUCKET_NAME,
                    Key: fileName,
                    Body: fileBuffer,
                    ACL: 'public-read',
                };
                s3bucket.upload(params, (err, data) => {
                    if (err) {
                        console.log(err)
                        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_UPLOAD_MEDIA, AppConstant.SUCCESS);

                    }
                    console.log(data)
                    const media = new Media();
                    media.mediaUrl = data.Location;
                    media.name = data.Key;
                    media.ETag = data.ETag.toString();
                    media.Bucket = data.Bucket.toString();
                    media.type = files.file.type;
                    media.addedOn = Date.now();
                    UserManager.saveMedia(media);
                    return Utility.response(res, {mediaUrl: data.Location}, AppConstant.MESSAGE.FILE_UPLOADED_SUCCESSFULLY, AppConstant.SUCCESS);
                });
            });
        });
    } catch (e) {
        console.log(e);
        return Utility.response(res, {}, AppConstant.MESSAGE.UNABLE_TO_UPLOAD_MEDIA, AppConstant.FAILURE);
    }
};
