const AppConstant = require('../utility/constant');
const AWS = require('aws-sdk');
module.exports = {
    response,
    sendSms,
    sendPush,
    sendMail
};


function response(res, data, message, status) {
    const responseData = {
        responseData: data,
        message,
        status,
    };
    res.status(status);
    res.format({
        json: () => {
            res.json(responseData);
        },
    });
}

function sendPush(pushObj) {
    console.log(pushObj)
    const message = {
        data: pushObj.data,
        notification: {
            title: pushObj.title,
            body: pushObj.body
        }
    };
    if (pushObj.caseId) {
        message.data.caseId = pushObj.caseId
    }
    console.log("*******************************")
    console.log(message)
    console.log("*******************************")
    admin.messaging().sendToDevice(pushObj.deviceToken, message)
        .then((response) => {
            // Response is a message ID string.
            console.log('Successfully sent message:', response);
        })
        .catch((error) => {
            console.log('Error sending message:', error);
        });
}

function sendSms(otpObj) {
    AWS.config.region = 'ap-south-1';
    AWS.config.update({
        accessKeyId: AppConfig.USER_KEY,
        secretAccessKey: AppConfig.USER_SECRET,
        region: 'ap-south-1',
    });

    // const sns = new AWS.SNS();
    const params = {
        Message: otpObj.body,
        PhoneNumber: otpObj.phone,
        MessageAttributes: {
            'AWS.SNS.SMS.SMSType': {
                DataType: 'String',
                StringValue: 'Transactional',
            },
        }
    };
    // Create promise and SNS service object
    const publishTextPromise = new AWS.SNS({apiVersion: '2010-03-31'}).publish(params).promise();

    // Handle promise's fulfilled/rejected states
    publishTextPromise.then(
        (data) => {
            console.log(`MessageID is ${data.MessageId}`);
        },
    ).catch(
        (err) => {
            console.error(err, err.stack);
        },
    );
// snippet-end:[sns.JavaScript.SMS.publish]
}

function sendMail(emailObj) {
    console.log("Iam alcla")
    AWS.config.update({region: 'us-west-2'});
    AWS.config.update({
        accessKeyId: AppConfig.USER_KEY,
        secretAccessKey: AppConfig.USER_SECRET,
        region: 'us-west-2',
    });

// Create sendEmail params
    console.log(emailObj.email)
    const params = {
        Destination: { /* required */
            CcAddresses: [
                /* more items */
            ],
            ToAddresses: [emailObj.email]
        },
        Message: { /* required */
            Body: { /* required */
                Html: {
                    Charset: "UTF-8",
                    Data: emailObj.body
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: emailObj.subject
            }
        },
        Source: AppConstant.CONTACT.EMAIL, /* required */
        ReplyToAddresses: [],
    };
    // console.log(params)
// Create the promise and SES service object
//     objectnew AWS.SES({apiVersion: '2010-12-01'}).sendEmail(params).promise();
//     const sezndPromise = new AWS.SES({apiVersion: '2010-12-01'}).verifyEmailAddress({
//         EmailAddress: emailObj.email
//     }, ((err, data) => {
//         console.log(data)
//         console.log(err)
    new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(params).promise().then(
        function (data) {
            console.log(data.MessageId);
        }).catch(
        function (err) {
            console.error(err, err.stack);
        });
    // }))

// Handle promise's fulfilled/rejected states
//     sendPromise.then(
//         function (data) {
//             console.log(data.MessageId);
//         }).catch(
//         function (err) {
//             console.error(err, err.stack);
//         });
// snippet-end:[ses.JavaScript.email.sendEmail]
}
